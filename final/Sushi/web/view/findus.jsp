<%-- 
    Document   : findus
    Created on : Jan 7, 2019, 12:50:07 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="mt" uri="/WEB-INF/tlds/static"%>

<t:_layout>
    <jsp:attribute name="body">
        <div class="findus">
            <div class="block__title">Find us</div>
            <div class="findus__info">
                <div class="address">
                    <div class="address__title">Address and contact:</div>
                    <div class="address__content">
                        <div>The Sushi Restaurant</div>
                        <div>New York, NY, USA</div><br>
                        <div><span class="feild">Tel:</span>
                            <content>12345</content>
                        </div><br>
                        <div> <span class="feild">Email:</span>
                            <content>your-email@your-email.com</content>
                        </div>
                    </div>
                </div>
                <div class="opening">
                    <div class="opening__title">Opening hours:</div>
                    <div class="opening__content">
                        <div>Monday Closed</div>
                        <div>Tuesday 12 - 22</div>
                        <div>Wednesday 12 - 22</div>
                        <div>Thursday 12 - 22</div>
                        <div>Friday 11 - 23</div>
                        <div>Saturday 11 - 23</div>
                        <div>Sunday 11 - 22</div>
                    </div>
                </div>
            </div>
            <div class="break-dash">
                <mt:st cls="findus__map" type="img" path="map.png"/>
            </div>
        </div>
    </jsp:attribute>
</t:_layout>

