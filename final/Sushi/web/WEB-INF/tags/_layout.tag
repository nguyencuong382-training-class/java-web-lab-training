<%-- 
    Document   : _layout
    Created on : Jan 6, 2019, 11:52:51 PM
    Author     : Admin
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="mt" uri="/WEB-INF/tlds/static"%>
<%@attribute name="body" fragment="true" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Sushi Restaurant</title>
        <mt:st type="css" path="share.css"/>
        <mt:st type="css" path="header.css"/>
        <mt:st type="css" path="menu.css"/>
        <mt:st type="css" path="content.css"/>
        <mt:st type="css" path="findus.css"/>
        <mt:st type="css" path="footer.css"/>
    </head>
    <body>
        <div class="outer">
            <div class="container">
                <div class="header">
                    <div class="title-wrapper">
                        <div class="title-wrapper-inner">
                            <div class="title main-color">The Sushi Restaurant</div>
                            <div class="subtitle">Welcome to this website</div>
                        </div>
                    </div>
                    <div class="nav">
                        <div class="nav-inner">
                            <ul>
                                <li> <a class="${active == "home" ? 'active':''}" href="<mt:st/>/home">Home</a></li>
                                <li> <a class="${active == "menu" ? 'active':''}" href="<mt:st/>/menu">Menu and Price list</a></li>
                                <li> <a class="${active == "findus" ? 'active':''}" href="<mt:st/>/findus">Find us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="content">
                        <div class="content-inner">
                            <div class="block">
                                <jsp:invoke fragment="body"/>
                            </div>
                            <c:if test="${not empty links}">
                                <div class="share-page">
                                    <div class="share-page__content">
                                        <div class="title">Share this page</div>
                                        <ul>
                                            <c:forEach var="link" items="${links}">
                                                <li><a href="${link.link}"><mt:st type="img" path="${link.img}"/><span>${link.name}</span></a></li>
                                                        </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="inner">
                        <div class="break-solid"></div>
                    </div>
                    <div class="footer-inner"><a href="#">Created with SimpleSite</a>
                        <div class="time">
                            <span>1</span>
                            <span>2</span>
                            <span>3</span>
                            <span>2</span>
                            <span>2</span>
                            <span>2</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>