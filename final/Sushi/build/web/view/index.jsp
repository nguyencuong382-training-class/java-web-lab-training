<%-- 
    Document   : index
    Created on : Oct 22, 2018, 9:38:04 AM
    Author     : smart
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="mt" uri="/WEB-INF/tlds/static"%>

<c:if test="${empty servlet}">
    <jsp:forward page="/" />
</c:if>

<t:_layout>
    <jsp:attribute name="body">
        <div class="intro">
            <div class="intro__image">
                <mt:st type="img" path="1.jpg"/>
            </div>
            <div class="break-dash"></div>
            <div class="intro__type">
                <c:if test="${not empty menu && menu.sushies.size() > 0}">
                    <c:forEach var="sushi" items="${menu.sushies}" varStatus="loop">
                        <div class="intro__type__item">
                            <div class="title"><a href="${menu.currentUrl}">${sushi.name}</a></div>
                            <div class="content">
                                <p>
                                    <mt:st type="img" path="${sushi.img}"/>
                                    <c:if test="${not empty param.detail && param.detail == true}">
                                        ${sushi.detail}
                                    </c:if>

                                    <c:if test="${empty param.detail || param.detail == false}">
                                        ${sushi.description}
                                    </c:if>

                                </p>
                            </div>
                        </div>

                    </c:forEach>

                    <div class="pagin">
                        <a class="prev ${menu.prev == null ? 'not-active':''}" href="${menu.prev}">Prev</a>
                        <a class="next ${menu.next == null ? 'not-active':''}" href="${menu.next}">Next</a>
                    </div>
                </c:if>
            </div>
        </div>
    </jsp:attribute>
</t:_layout>

