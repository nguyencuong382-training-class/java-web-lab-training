<%-- 
    Document   : menu
    Created on : Jan 7, 2019, 12:47:19 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:_layout>
    <jsp:attribute name="body">

        <div class="menu">
            <div class="block__title">Menu and Price list</div>
            <c:if test="${not empty menu && menu.sushies.size() > 0}">
                <c:forEach var="sushi" items="${menu.sushies}" varStatus="loop">
                    <div class="menu__item">
                        <div class="break-solid"></div>
                        <div class="title">
                            <div>Menu ${sushi.id}</div>
                            <div>Price</div>
                        </div>
                        <div class="break-solid"></div>
                        <div class="sushi">
                            <div class="name">${sushi.name}</div>
                            <div class="price">€${sushi.price}</div>
                        </div>
                        <div class="description">${sushi.description}</div>
                        <c:if test="${loop.index < shushies.size() - 1}">
                            <div class="break-dash"></div>
                        </c:if>
                    </div>
                </c:forEach>
                <div class="pagin">
                    <a class="prev ${menu.prev == null ? 'not-active':''}" href="${menu.prev}">Prev</a>
                    <a class="next ${menu.next == null ? 'not-active':''}" href="${menu.next}">Next</a>
                </div>
            </c:if>
        </div>

    </jsp:attribute>
</t:_layout>

