/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Sushi;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import middleware.LinkShareMiddleware;
import model.SushiDAO;
import pagination.MenuPagination;
import util.AppContext;

/**
 *
 * @author Admin
 */
public class MenuController extends HttpServlet {

    AppContext ac = new AppContext();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Sushi> sushies = null;

        String next_ = request.getParameter("next");
        String prev_ = request.getParameter("prev");
        next_ = (next_ != null && next_.equals("")) ? null : next_;
        prev_ = (prev_ != null && prev_.equals("")) ? null : prev_;

        try {
            int size = Integer.parseInt(ac.env("pageSize"));
            MenuPagination mp = new Pagination().getMenuPagination(size, next_, prev_, "menu");
            request.setAttribute("menu", mp);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        LinkShareMiddleware.addShare(request, response);
        request.setAttribute("active", "menu");
        RequestDispatcher rd = request.getRequestDispatcher("/view/menu.jsp");
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
