/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Sushi;
import java.util.Collections;
import java.util.List;
import model.SushiDAO;
import pagination.MenuPagination;
import util.AppContext;

/**
 *
 * @author Admin
 */
public class Pagination {

    AppContext ac = new AppContext();
    SushiDAO sdao = new SushiDAO();

    public MenuPagination getMenuPagination(int size, String next_, String prev_, String path) throws Exception {
        List<Sushi> sushies;

        int id = next_ != null ? Integer.parseInt(next_) : -1;
        String prev = null, next = null;
        MenuPagination mp = null;

        if (prev_ != null) {
            id = Integer.parseInt(prev_);
            sushies = sdao.getPrev(id, size);
            Collections.reverse(sushies);
        } else {
            sushies = sdao.getNext(id, size);
        }

        if (sushies.size() > 0) {
            if (sdao.getFirst().getId() != sushies.get(0).getId()) {
                prev = ac.env("contextPath") + "/"+path+"?prev=" + sushies.get(0).getId();
            }

            if (sdao.getLast().getId() != sushies.get(sushies.size() - 1).getId()) {
                next = ac.env("contextPath") + "/"+path+"?next=" + sushies.get(sushies.size() - 1).getId();
            }

            mp = new MenuPagination(prev, next, sushies);
        }

        return mp;
    }
}
