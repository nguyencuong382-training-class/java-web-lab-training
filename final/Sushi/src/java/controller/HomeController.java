/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import util.AppContext;
import entity.Post;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpUtils;
import middleware.LinkShareMiddleware;
import model.PostDAO;
import model.SushiDAO;
import pagination.MenuPagination;

/**
 *
 * @author Admin
 */
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Post> posts = null;
        
        
        StringBuffer currentUrl = request.getRequestURL().append('?');
        String query = request.getQueryString();
        
        if(request.getParameter("detail") == null) {
            if(query == null) {  
                currentUrl.append("detail=true");
            } else {
                currentUrl.append(query);
                currentUrl.append("&detail=true");
            }
        } else {
            currentUrl.append(query);
        }

        String next_ = request.getParameter("next");
        String prev_ = request.getParameter("prev");
        next_ = (next_ != null && next_.equals("")) ? null : next_;
        prev_ = (prev_ != null && prev_.equals("")) ? null : prev_;
       
        
        
        try {
            MenuPagination mp = new Pagination().getMenuPagination(3, next_, prev_, "home");
            mp.setCurrentUrl(currentUrl.toString());
            request.setAttribute("menu", mp);
            request.setAttribute("servlet", true);
            LinkShareMiddleware.addShare(request, response);
        } catch (Exception e) {
            System.out.println(e);
        }
        request.setAttribute("active", "home");
        RequestDispatcher rd = request.getRequestDispatcher("/view/index.jsp");
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
