/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import context.DBContext;
import entity.Sushi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class SushiDAO extends DAO {

    public List<Sushi> list(String query) throws Exception {
        List<Sushi> shushies = null;
        
        Connection conn = null;
                
        try {
            conn = DBContext.getInstace().getConnection();
            shushies = new ArrayList<>();
            PreparedStatement ps = conn
                    .prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("Id");;
                double price = resultSet.getFloat("price");
                String name = resultSet.getString("name");
                String img = resultSet.getString("img");
                String description = resultSet.getString("description");

                shushies.add(new Sushi(id, price, name, img, description, "a" + description));
            }
        } catch (Exception ex) {
            throw ex;
        }
        this.close(conn);
        return shushies;
    }

    public Sushi getFirst() throws Exception {
        String query = "select top 1 * from Sushi order by id";
        return list(query).get(0);
    }
    
    public Sushi getLast() throws Exception {
        String query = "select top 1 * from Sushi order by id desc";
        return list(query).get(0);
    }
    
    public List<Sushi> getNext(int currentId, int limit) throws Exception {
        String query = "select top "+limit+" * from Sushi where id > "+currentId+" order by id";
        return list(query);
    }
    
    public List<Sushi> getPrev(int currentId, int limit) throws Exception {
        String query = "select top "+limit+" * from Sushi where id < "+currentId+" order by id desc";
        return list(query);
    }
    
     public Sushi getSushi(int id) throws Exception {
        String query = "select * from Sushi where id = " + id;
        return list(query).get(0);
    }

}
