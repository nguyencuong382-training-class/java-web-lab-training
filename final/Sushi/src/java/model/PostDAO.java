/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import context.DBContext;
import entity.Post;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class PostDAO extends DAO {

    public List<Post> list() throws Exception {
        List<Post> posts = new ArrayList<>();
        Connection conn = null;
        try {
            conn = DBContext.getInstace().getConnection();
            String query = "select * from Post";
            PreparedStatement ps = conn
                    .prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("Id");
                String name = resultSet.getString("name");
                String img = resultSet.getString("img");
                String description = resultSet.getString("description");

                posts.add(new Post(id, name, img, description));
            }

            ps.close();
            resultSet.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        this.close(conn);

        return posts;
    }

}
