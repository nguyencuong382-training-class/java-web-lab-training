/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import context.DBContext;
import entity.Link;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class LinkDAO {

    public List<Link> list() throws Exception {
        List<Link> links;
        try (Connection conn = DBContext.getInstace().getConnection()) {
            links = new ArrayList<>();
            String query = "select * from Link";

            PreparedStatement ps = conn
                    .prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");;
                String name = resultSet.getString("name");
                String link = resultSet.getString("link");
                String img = resultSet.getString("img");
                
                links.add(new Link(id, name, link, img));
            }
        }

        return links;
    }
    
}
