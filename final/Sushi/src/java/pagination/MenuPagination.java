package pagination;

import entity.Sushi;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
public class MenuPagination {
    private String prev;
    private String next;
    private String currentUrl;
    
    private List<Sushi> sushies;

    public MenuPagination() {
    }

    public MenuPagination(String prev, String next, List<Sushi> sushies) {
        this.prev = prev;
        this.next = next;
        this.sushies = sushies;
    }

    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public String getCurrentUrl() {
        return currentUrl;
    }

    

    public String getPrev() {
        return prev;
    }

    public void setPrev(String prev) {
        this.prev = prev;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<Sushi> getSushies() {
        return sushies;
    }

    public void setSushies(List<Sushi> sushies) {
        this.sushies = sushies;
    }
    
    
}
