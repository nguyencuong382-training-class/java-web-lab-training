## setup environment
1. setup nodejs https://nodejs.org/en/download/
2. setup yarn https://yarnpkg.com/lang/en/docs/install/#windows-stable
3. setup git https://git-scm.com/downloads
4. clone project by command
```
git clone https://gitlab.com/training-class/java-web-lab-training
```
5. Setup modules for bower project
```
cd java_web_lab_training/font-end/
npm install -g bower
npm install -g gulp
yarn install
bower install
```
6. Run project
```
gulp serve
```

7. Pull code
```
git pull origin master
```