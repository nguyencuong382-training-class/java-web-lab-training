package org.apache.jsp.tag.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class _005flayout_tag
    extends javax.servlet.jsp.tagext.SimpleTagSupport
    implements org.apache.jasper.runtime.JspSourceDependent {


  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private JspContext jspContext;
  private java.io.Writer _jspx_sout;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public void setJspContext(JspContext ctx) {
    super.setJspContext(ctx);
    java.util.ArrayList<String> _jspx_nested = null;
    java.util.ArrayList<String> _jspx_at_begin = null;
    java.util.ArrayList<String> _jspx_at_end = null;
    this.jspContext = new org.apache.jasper.runtime.JspContextWrapper(ctx, _jspx_nested, _jspx_at_begin, _jspx_at_end, null);
  }

  public JspContext getJspContext() {
    return this.jspContext;
  }
  private javax.servlet.jsp.tagext.JspFragment body;

  public javax.servlet.jsp.tagext.JspFragment getBody() {
    return this.body;
  }

  public void setBody(javax.servlet.jsp.tagext.JspFragment body) {
    this.body = body;
  }

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  private void _jspInit(ServletConfig config) {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(config);
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(config);
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_if_test.release();
  }

  public void doTag() throws JspException, java.io.IOException {
    PageContext _jspx_page_context = (PageContext)jspContext;
    HttpServletRequest request = (HttpServletRequest) _jspx_page_context.getRequest();
    HttpServletResponse response = (HttpServletResponse) _jspx_page_context.getResponse();
    HttpSession session = _jspx_page_context.getSession();
    ServletContext application = _jspx_page_context.getServletContext();
    ServletConfig config = _jspx_page_context.getServletConfig();
    JspWriter out = jspContext.getOut();
    _jspInit(config);
    if( getBody() != null ) {
      _jspx_page_context.setAttribute("body", getBody());
}

    try {
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Sushi Restaurant</title>\n");
      out.write("        ");
      if (_jspx_meth_mt_st_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_mt_st_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_mt_st_2(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_mt_st_3(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_mt_st_4(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_mt_st_5(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"header\">\n");
      out.write("                <div class=\"title-wrapper\">\n");
      out.write("                    <div class=\"title-wrapper-inner\">\n");
      out.write("                        <div class=\"title main-color\">The Sushi Restaurant</div>\n");
      out.write("                        <div class=\"subtitle\">Welcome to this website</div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"nav\">\n");
      out.write("                    <div class=\"nav-inner\">\n");
      out.write("                        <ul>\n");
      out.write("                            <li> <a class=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${active == \"home\" ? 'active':''}", java.lang.String.class, (PageContext)this.getJspContext(), null));
      out.write("\" href=\"");
      if (_jspx_meth_mt_st_6(_jspx_page_context))
        return;
      out.write("/home\">Home</a></li>\n");
      out.write("                            <li> <a class=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${active == \"menu\" ? 'active':''}", java.lang.String.class, (PageContext)this.getJspContext(), null));
      out.write("\" href=\"");
      if (_jspx_meth_mt_st_7(_jspx_page_context))
        return;
      out.write("/menu\">Menu and Price list</a></li>\n");
      out.write("                            <li> <a class=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${active == \"findus\" ? 'active':''}", java.lang.String.class, (PageContext)this.getJspContext(), null));
      out.write("\" href=\"");
      if (_jspx_meth_mt_st_8(_jspx_page_context))
        return;
      out.write("/findus\">Find us</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"content\">\n");
      out.write("                    <div class=\"content-inner\">\n");
      out.write("                        <div class=\"block\">\n");
      out.write("                            ");
      ((org.apache.jasper.runtime.JspContextWrapper) this.jspContext).syncBeforeInvoke();
      _jspx_sout = null;
      if (getBody() != null) {
        getBody().invoke(_jspx_sout);
      }
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                        ");
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"footer\">\n");
      out.write("                <div class=\"inner\">\n");
      out.write("                    <div class=\"break-solid\"></div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"footer-inner\"><a href=\"#\">Created with SimpleSite</a>\n");
      out.write("                    <div class=\"time\">\n");
      out.write("                        <span>1</span>\n");
      out.write("                        <span>2</span>\n");
      out.write("                        <span>3</span>\n");
      out.write("                        <span>2</span>\n");
      out.write("                        <span>2</span>\n");
      out.write("                        <span>2</span>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch( Throwable t ) {
      if( t instanceof SkipPageException )
          throw (SkipPageException) t;
      if( t instanceof java.io.IOException )
          throw (java.io.IOException) t;
      if( t instanceof IllegalStateException )
          throw (IllegalStateException) t;
      if( t instanceof JspException )
          throw (JspException) t;
      throw new JspException(t);
    } finally {
      ((org.apache.jasper.runtime.JspContextWrapper) jspContext).syncEndTagFile();
      _jspDestroy();
    }
  }

  private boolean _jspx_meth_mt_st_0(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_0.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_0.setType("css");
    _jspx_th_mt_st_0.setPath("share.css");
    _jspx_th_mt_st_0.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_1(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_1 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_1.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_1.setType("css");
    _jspx_th_mt_st_1.setPath("header.css");
    _jspx_th_mt_st_1.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_2(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_2 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_2.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_2.setType("css");
    _jspx_th_mt_st_2.setPath("menu.css");
    _jspx_th_mt_st_2.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_3(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_3 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_3.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_3.setType("css");
    _jspx_th_mt_st_3.setPath("content.css");
    _jspx_th_mt_st_3.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_4(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_4 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_4.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_4.setType("css");
    _jspx_th_mt_st_4.setPath("findus.css");
    _jspx_th_mt_st_4.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_5(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_5 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_5.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_5.setType("css");
    _jspx_th_mt_st_5.setPath("footer.css");
    _jspx_th_mt_st_5.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_6(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_6 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_6.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_6.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_7(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_7 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_7.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_7.doTag();
    return false;
  }

  private boolean _jspx_meth_mt_st_8(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_8 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_8.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_8.doTag();
    return false;
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${not empty links}", java.lang.Boolean.class, (PageContext)this.getJspContext(), null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                            <div class=\"share-page\">\n");
        out.write("                                <div class=\"share-page__content\">\n");
        out.write("                                    <div class=\"title\">Share this page</div>\n");
        out.write("                                    <ul>\n");
        out.write("                                        ");
        if (_jspx_meth_c_forEach_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_0, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("                                    </ul>\n");
        out.write("                                </div>\n");
        out.write("                            </div>\n");
        out.write("                        ");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      throw new SkipPageException();
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_forEach_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_0, PageContext _jspx_page_context)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_0);
    _jspx_th_c_forEach_0.setVar("link");
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${links}", java.lang.Object.class, (PageContext)this.getJspContext(), null));
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                            <li><a href=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${link.link}", java.lang.String.class, (PageContext)this.getJspContext(), null));
          out.write('"');
          out.write('>');
          if (_jspx_meth_mt_st_9((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("<span>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${link.name}", java.lang.String.class, (PageContext)this.getJspContext(), null));
          out.write("</span></a></li>\n");
          out.write("                                        ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        throw new SkipPageException();
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_mt_st_9(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    JspWriter out = _jspx_page_context.getOut();
    //  mt:st
    tags.Static _jspx_th_mt_st_9 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(tags.Static.class) : new tags.Static();
    _jspx_th_mt_st_9.setJspContext(_jspx_page_context);
    _jspx_th_mt_st_9.setParent(_jspx_th_c_forEach_0);
    _jspx_th_mt_st_9.setType("img");
    _jspx_th_mt_st_9.setPath((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${link.img}", java.lang.String.class, (PageContext)this.getJspContext(), null));
    _jspx_th_mt_st_9.doTag();
    return false;
  }
}
