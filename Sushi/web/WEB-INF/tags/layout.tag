<%-- 
    Document   : layout
    Created on : Feb 21, 2019, 8:24:21 PM
    Author     : Admin
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="content" fragment="true" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Sushi Restaurant</title>
        
        
        <link rel="stylesheet" href="/Sushi/static/css/share.css"/>
        <link rel="stylesheet" href="/Sushi/static/css/header.css"/>
        <link rel="stylesheet" href="/Sushi/static/css/content.css"/>
        <link rel="stylesheet" href="/Sushi/static/css/menu.css"/>
        <link rel="stylesheet" href="/Sushi/static/css/findus.css"/>
        <link rel="stylesheet" href="/Sushi/static/css/footer.css"/>
        
    </head>
    <body>

        <div class="container">
            <div class="header">
                <div class="title-wrapper">
                    <div class="title-wrapper-inner">
                        <div class="title main-color">The Sushi Restaurant</div>
                        <div class="subtitle">Welcome to this website</div>
                    </div>
                </div>
                <div class="nav">
                    <div class="nav-inner">
                        <ul>
                            <li><a class="active" href="/index.html">Home</a></li>
                            <li> <a href="/menu.html">Menu and Price list</a></li>
                            <li> <a href="/findus.html">Find us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="content-inner">
                    <div class="block">
                        <jsp:invoke fragment="content"/>
                    </div>

                    <div class="share-page">
                        <div class="share-page__content">
                            <div class="title">Share this page</div>
                            <ul>
                                <li><a href="#"><img src="/Sushi/static/imgs/1.jpg"><span>Share on Facebook</span></a></li>
                                <li><a href="#"><img src="/Sushi/static/imgs/2.jpg"><span>Share on Twitter</span></a></li>
                                <li><a href="#"> <img src="/Sushi/static/imgs/1.jpg"><span>Share on Google+</span></a></li>
                            </ul>
                        </div>
                        <div class="share-page__content">
                            <div class="title">Share this page</div>
                            <ul>
                                <li><a href="#"><img src="/Sushi/static/imgs/1.jpg"><span>Share on Facebook</span></a></li>
                                <li><a href="#"><img src="/Sushi/static/imgs/2.jpg"><span>Share on Twitter</span></a></li>
                                <li><a href="#"> <img src="/Sushi/static/imgs/1.jpg"><span>Share on Google+</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="inner">
                    <div class="break-solid"></div>
                </div>
                <div class="footer-inner"><a href="#">Created with SimpleSite</a>
                    <div class="time"><span>1</span><span>2</span><span>3</span><span>2</span><span>2</span><span>2</span></div>
                </div>
            </div>
        </div>
    </body>
</html>