<%-- 
    Document   : index.jsp
    Created on : Feb 21, 2019, 8:34:02 PM
    Author     : Admin
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:layout>
    <jsp:attribute name="content">




        <div class="intro">
            <div class="intro__image"><img src="/Sushi/static/imgs/1.jpg"></div>
            <div class="break-dash"></div>
            <div class="intro__type">


                <c:if test="${not empty items}">

                    <c:forEach var="item" items="${items}">
                        <div class="intro__type__item">
                            <div class="title"><a href="/Sushi/sushi?id=${item.id}">${item.title}</a></div>
                            <div class="content">
                                <p><img src="/Sushi/static/imgs/${item.img}">
                                    
                                    ${item.shortDescription}
                                </p>
                            </div>
                        </div>
                    </c:forEach>
                </c:if>    

                <c:if test="${empty items}">
                    <h1>There is no Data!</h1>
                </c:if>

                <div class="pagin"><a class="prev not-active" href="">Prev</a><a class="next" href="">Next</a></div>
            </div>
        </div>

    </jsp:attribute>
</t:layout>