package context;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import util.AppContext;

public class DBContext {

    private String url;
    private static DBContext dbContext;
    AppContext ac = new AppContext();

    public static DBContext getInstace() throws Exception {
        if (dbContext == null) {
            dbContext = new DBContext();
        }
        return dbContext;
    }

    public Connection getConnection() throws Exception {
        Connection con = null;
        try {
            con = DriverManager.getConnection(url);
        } catch (Exception e) {
            url = ac.getDirectValue("urlConnection");
            con = DriverManager.getConnection(url);
        }
        return con;
    }

    private DBContext() throws Exception {
        url = ac.env("urlConnection");
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

    }

}