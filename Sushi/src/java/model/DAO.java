/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Admin
 */
public class DAO {
    public void close(Connection con, ResultSet rs, PreparedStatement ps) throws Exception {
        
        if(ps != null && !ps.isClosed()) {
            ps.close();
        }
        
        if(rs != null && !rs.isClosed()) {
            rs.close();
        }
        
        if(con != null && !con.isClosed()) {
            con.close();
        }
    }
}
