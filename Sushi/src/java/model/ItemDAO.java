/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import context.DBContext;
import entity.Item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ItemDAO extends DAO {

    public List<Item> getAll() throws Exception {
        List<Item> items = null;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;

        try {
            conn = DBContext.getInstace().getConnection();

            items = new ArrayList<>();
            String query = "select * from Item";

            ps = conn.prepareStatement(query);
            resultSet = ps.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");;
                String title = resultSet.getString("title");
                String shortDescription = resultSet.getString("shortDescription");
                String description = resultSet.getString("description");
                String img = resultSet.getString("img");

                items.add(new Item(id, title, shortDescription, description, img));

            }

            this.close(conn, resultSet, ps);
        } catch (Exception e) {
            this.close(conn, resultSet, ps);
            throw e;
        }

        return items;
    }
}
